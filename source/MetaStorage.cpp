
#include "except.h"
#include "MetaStorage.h"

MetaStorage::MetaStorage()
{
	_log = spdlog::stdout_logger_mt("meta-storage");
}

MetaStorage::~MetaStorage()
{
}

void MetaStorage::connect(std::string mongo_connection_string)
{
	auto r = mongo::client::initialize();

	if (!r.isOK())
	{
		throw SystemExit(fmt::format("failed to initizlize mongo driver ({}): {} @{}", r.codeString(), r.reason(), r.location()));
	}

	_mongo.connect(mongo_connection_string);
}

mongo::BSONObj MetaStorage::find(const mongo::BSONObj & query)
{
	auto s = _mongo.findOne("db.segments", query);

	_log->debug("quering {}", query.jsonString());

	return s;
}
