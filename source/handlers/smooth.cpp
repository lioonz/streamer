
#include <spdlog/spdlog.h>
#include <isom/isom.h>
#include "smooth.h"

const boost::regex SmoothHandler::URL_REGEX = boost::regex("/v0/pl/ss/standard/none/(\\w+)\\.ism(l?)/QualityLevels\\((\\d+)\\)/Fragment(s|Info)\\(track(\\d+)=(\\d+)\\)");

class SmoothHandler::UrlInfo
{
public:
	std::string resource_id;
	bool live;
	unsigned bitrate;
	unsigned track_id;
	uint64_t time;
	bool fragment_info;

	std::string str()
	{
		return fmt::format("r={} l={} b={} t={} time={}", resource_id, live, bitrate, track_id, time);
	}
};

bool SmoothHandler::parse_request(std::string uri, UrlInfo & ui)
{
	boost::smatch m;
	auto r = boost::regex_match(uri, m, URL_REGEX);

	if(!r)
	{
		return false;
	}

	assert(m.size() == 7);

	ui.resource_id = m[1];
	ui.live = m[2] == "l";
	ui.bitrate = stoi(m[3]);
	ui.fragment_info = m[4] == "Info";
	ui.track_id = stoi(m[5]);
	ui.time = stoll(m[6]);

	return r;
}

SmoothHandler::SmoothHandler(MetaStoragePtr meta_storage, BlobStoragePtr blob_storage)
{
	_meta_storage = meta_storage;
	_blob_storage = blob_storage;
	_log = spdlog::stdout_logger_mt("smooth");
}

int SmoothHandler::handle(struct mg_connection * conn, boost::smatch)
{
	auto ui = UrlInfo();
	auto r = parse_request(conn->uri, ui);

	if(!r)
	{
		mg_send_status(conn, 400);
		mg_printf_data(conn, "failed to parse smooth uri", 123);
		return MG_TRUE;
	}

	auto q = BSON(
		"resource_id" << ui.resource_id <<
		"track_id"    << ui.track_id    <<
		"quality_id"  << ui.bitrate     <<
		"time"        << mongo::GTE << (long long int) ui.time
	);

	auto meta = _meta_storage->find(q);

	if(meta.isEmpty())
	{
		mg_send_status(conn, 404);
		mg_printf_data(conn, "Not Found");
		return MG_TRUE;
	}

	std::vector<uint8_t> blob;
	auto sid = meta["segment_id"].String();
	r = _blob_storage->find(sid, blob);

	if(!r)
	{
		_log->warn("failed to find segment blob for segment '{}' when meta found", meta["segment_id"]);
		mg_send_status(conn, 404);
		mg_printf_data(conn, "Not Found");
		return MG_TRUE;
	}

	insert_live_boxes(meta, blob, ui.fragment_info);

	mg_send_header(conn, "Content-Type", "video/mp4");
	mg_send_data(conn, blob.data(), blob.size());

	return MG_TRUE;
}

void SmoothHandler::insert_live_boxes(mongo::BSONObj meta, Blob & blob, bool headers_only)
{
	auto q = BSON(
		"resource_id" << meta["resource_id"].String() <<
		"track_id"    << meta["track_id"].Int()    <<
		"quality_id"  << meta["quality_id"].Int()     <<
		"index"       << mongo::GT << meta["index"].Long()
	);

	auto next_meta = _meta_storage->find(q);

	if(next_meta.isEmpty())
	{
		_log->warn("failed to add live boxes to segment: did not find next segment in meta storage");
		return;
		// throw std::runtime_error("no next segment");
	}

	isom_insert_boxes(blob, meta["time"].Long(), meta["duration"].Long(), next_meta["time"].Long(), next_meta["duration"].Long(), headers_only);
}
