
#include <string>
#include <regex>
#include <thread>
#include <csignal>
#include <mongoose.h>
#include <spdlog/spdlog.h>
#include "except.h"
#include "log.h"
#include "httpsrv.h"
#include "handlers/smooth.h"
#include "handlers/hlsv3.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libavutil/avutil.h>
}


static auto logger = spdlog::stdout_logger_mt("app");
HttpServer srv;

class Config
{
public:

	static Config parse_args(int argc, char * argv[]);

	std::string mongo_connection;
	std::string ceph_config;
};

Config Config::parse_args(int argc, char * argv[])
{
	enforce<UsageExit>(argc == 3, "usage: <mongo-connection> <ceph-config-file>");

	Config c;
	c.mongo_connection = argv[1];
	c.ceph_config = argv[2];

	return c;
}

void terminate(int s)
{
	logger->info("signal {} received: terminating ...", s);
	srv.stop();
}

void run(int argc, char * argv[])
{
	av_register_all();

	auto c = Config::parse_args(argc, argv);

	logger->info("parsed cmd args");

	signal(SIGINT, &terminate);
	signal(SIGTERM, &terminate);

	auto meta_storage = std::make_shared<MetaStorage>();
	auto blob_storage = std::make_shared<BlobStorage>();

	meta_storage->connect(c.mongo_connection);
	blob_storage->connect(c.ceph_config, "");

	SmoothHandler smooth(meta_storage, blob_storage);
	Hlsv3Handler hlsv3(meta_storage, blob_storage);

	srv.register_handler("/v0/pl/ss/.*", std::bind(&SmoothHandler::handle, &smooth, std::placeholders::_1, std::placeholders::_2));
	srv.register_handler("/v0/segs/ts/.*", std::bind(&Hlsv3Handler::handle, &hlsv3, std::placeholders::_1, std::placeholders::_2));

	srv.run(9000);
}

int main(int argc, char * argv[])
{
	auto fmt = std::make_shared<LogFormatter>();
	spdlog::set_formatter(std::dynamic_pointer_cast<spdlog::formatter>(fmt));
	spdlog::set_level(spdlog::level::debug);

	try
	{
		run(argc, argv);
		logger->notice("clean exit");
		return 0;
	}
	catch(UsageExit & e)
	{
		fmt::print("{}\n", e.msg());
		return e.code();
	}
	catch(SystemExit & e)
	{
		logger->error("{}", e.msg());
		return e.code();
	}
	catch(mongo::UserException & e)
	{
		logger->error("mongo error: {}", e.what());
		return 1;
	}
	catch(std::exception & e)
	{
		logger->error("exception caught: {}", e.what());
		return -1;
	}
}
