
#pragma once

#include <memory>
#include <vector>
#include <string>
#include <rados/librados.h>

typedef std::vector<uint8_t> Blob;

class BlobStorage
{
	rados_t cluster;
	rados_ioctx_t io;
	std::shared_ptr<spdlog::logger> _log;
public:
	BlobStorage();
	~BlobStorage();

	void connect(std::string ceph_conf, std::string ceph_keyring);

	bool find(const std::string & sid, Blob & blob);
};

typedef std::shared_ptr<BlobStorage> BlobStoragePtr;
